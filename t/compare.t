use strict;
use warnings;
use Test::More tests => 3;
require './duplicatecode.pl';
is(compare("Test", "Test"), 1);
is(compare("Abcd", "Efgh"), 0);
is(compare("Abcd", "defg"), 1/4);
