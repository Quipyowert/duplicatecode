#!/usr/bin/perl
use strict;
use warnings;
use diagnostics;
use File::Find;
use Fcntl qw(SEEK_SET SEEK_CUR);
use English qw(-no_match_vars);
use constant {
    LINE => 0,
    LINE2 => 1,
    FILE => 2,
    FILE2 => 3,
    LEN => 4,
};
{
    no warnings 'redefine';
    my $haveInline = 0;
    eval "use Inline 'C';\$haveInline = 1;";
    warn "Inline::C not available; using pure-Perl compare" if !$haveInline;
    Inline::init() if $haveInline;
}
#Catch Ctrl-C (INT) signal
my $requestexit=0;
my $threshold = 0.85;
#Only call main if not in a unit test
main() if not caller();
sub main {
    #This way NYTProf can read the profile output.
    local $SIG{INT} = \&quit;
    my @searchdirs;
    my @files;
    if (scalar @ARGV == 0) {
        print STDERR <<HELP;
duplicatecode.pl <filename|directory> ...
Tries to find duplicated blocks of code. Outputs a report of which lines are
duplicated. The highest scoring blocks are the first place to look.

Pass just a single filename to detect code copied just within that file.

Use --self, --compareself, -cs, -c, or -s to compare a file against itself.

Use --gui to start a Tk gui with the results.

Use --save results.txt (or File->Save in gui) to save results to file results.txt.
HELP
        exit 1;
    }
    my $compareself = 0;
    my $dashdashseen = 0;
    my $gui = 0;
    my $outputFile = undef;
    my $lookingForFile = 0;
    for my $arg (@ARGV) {
        if ($lookingForFile) {
            $outputFile = $arg;
            $lookingForFile = 0;
            next;
        }
        if (!$dashdashseen and $arg eq '--') {
            $dashdashseen = 1;
            next;
        }
        if (!$dashdashseen && ($arg eq '--compareself' || $arg eq '-cs' || $arg eq '--self' || $arg eq '-c' || $arg eq '-s')) {
            $compareself = 1;
            next;
        }
        if (!$dashdashseen && ($arg eq '--gui')) {
            $gui = 1;
            next;
        }
        if (!$dashdashseen && ($arg eq '--save')) {
            $lookingForFile = 1;
            next;
        }
        if (-d $arg) {
            push @searchdirs, $arg;
        }
        else {
            push @files, $arg;
        }
    }
    if ($lookingForFile) {
        die "--save takes an argument";
    }
    if (@searchdirs) {
        find({wanted => sub {push @files, $_ if -T}, no_chdir => 1}, @searchdirs);
    }
    if (!@searchdirs && scalar(@files) == 1) {
        #Comparing a file against itself
        $compareself = 1;
    }
    undef @searchdirs;
    print "Finding duplicate code\n";
    #line number in first file, line number in second file, 1st filename, 2nd filename, match length
    my @matches = finddupcode(\@files, $compareself);
    if ($gui) {
        gui(\@matches);
    }
    else {
        report(\@matches);
        if ($outputFile) {
            save(\@matches, $outputFile);
        }
    }
}
sub gui {
    my $tkAvailable = 0;
    eval 'use Tk;$tkAvailable = 1;';
    die "Tk module is not available but GUI requested" if !$tkAvailable;
    my ($matches) = @_;
    my $main = MainWindow->new();
    my %stringToMatch = map {matchToString($_) => $_} @$matches;
    my @matchesList = map {matchToString($_)} sort {$b->{score} <=> $a->{score}} @$matches;
    my $listboxSelected = 0;
    my $selectedFile = 0;
    my $selectedMatch;
    my $results = $main->Label(-text => "Results");
    my $listbox = $main->Scrolled('Listbox',
                                  -scrollbars => 'se',
                                  -listvariable => \@matchesList,
                                  -selectmode => "single");
    my $frame1 = $main->Frame();
    my $label1 = $frame1->Label(-text => "File 1");
    my $text = $frame1->LineNumberText("Text", -scrollbars => 'se'); #-text => "Test1");
    my $frame2 = $main->Frame();
    my $label2 = $frame2->Label(-text => "File 2");
    my $text2 = $frame2->LineNumberText("Text", -scrollbars => 'se'); #-text => "Test2");
    print "Listbox size: " . $listbox->size() . "\n";
    $text->bind('<FocusIn>', sub {
        $selectedFile = 1;
        $listboxSelected = 0;
    });
    $text2->bind('<FocusIn>', sub {
        $selectedFile = 2;
        $listboxSelected = 0;
    });
    $listbox->bind('<<ListboxSelect>>', sub {
        $listboxSelected = 1;
        my $index = @{$listbox->curselection()}[0];
        print "Index is $index\n";
        my $match = $stringToMatch{$listbox->get($index)};
        return unless defined $match;
        print("Match is $match\n");
        $selectedMatch = $match;
        my ($f1,$f2);
        #Clear both file views
        $text->Contents("");
        $text2->Contents("");
        open $f1, "<", $match->{file};
        while (defined($_ = <$f1>)) {
            $text->Insert($_);
        }
        $text->GotoLineNumber($match->{line});
        if ($match->{file} eq $match->{file2}) {
                $f2 = $f1;
                seek($f2, 0, SEEK_SET);
        }
        else {
                open $f2, "<", $match->{file2};
        }
        $. = 0;
        while (defined($_ = <$f2>)) {
            $text2->Insert($_);
        }
        $text2->GotoLineNumber($match->{line2});
        close $f2;
        close $f1;
    });
    $main->configure(-menu => my $menu = $main->Menu());
    my $save = sub {
        my ($contents, $file);
        if ($listboxSelected) {
            #Get contents of listbox then save to a file.
            my @contents = $listbox->get(0, $listbox->size());
            my $file = $main->getSaveFile();
            open my $fh, ">", $file or die "Can't save results to '$file': $!";
            print {$fh} map {"$_\n"} @contents;
            close $fh or die "Can't close '$file': $!";
            print "Successfully saved results to file '$file'\n";
            return;
        }
        if ($selectedFile == 1) {
            $contents = $text->Tk::Text::Contents();
            $file = $selectedMatch->{file};
        }
        elsif ($selectedFile == 2) {
            $contents = $text2->Tk::Text::Contents();
            $file = $selectedMatch->{file2};
        }
        else {
            print "Neither File 1 nor File 2 were selected.\n";
            return;
        }
        open my $output, ">", $file or die "Can't open $file for writing: $!";
        print $output $contents;
        close $output;
        print "Successfully saved $file\n";
    };
    my $open = sub {
        my $file = $main->getOpenFile();
        print "File '$file' was selected\n";
        $requestexit = 0;
        $matches = [finddupcode([$file], 1)];
        %stringToMatch = map {matchToString($_) => $_} @$matches;
        @matchesList = map{matchToString($_)} sort {$b->{score} <=> $a->{score}} @$matches;
    };
    my $filemenu = $menu->cascade(-label => '~File', -menuitems => [
        ['command', 'Open', -accelerator => "Ctrl-o", -command => $open],
        ['command', 'Save', -accelerator => "Ctrl-s", -command => $save],
        ['command', 'Quit', -accelerator => "Ctrl-q", -command => \&exit],
    ]);
    $main->bind('<Control-o>', $open);
    $main->bind('<Control-s>', $save);
    $main->bind('<Control-q>', \&exit);
    $results->pack(-side=>"top");
    $listbox->pack(-expand => 1, -fill => "x");
    $label1->pack();
    $label2->pack();
    $text->pack();
    $text2->pack();
    $frame1->pack(-side=>"left");
    $frame2->pack(-side=>"right");
    MainLoop();
}
sub matchToString {
    my ($match) = @_;
    my $string = "";
    if ($match->{len} == 1) {
        $string .= "Ln: $match->{line}"
           . " Ln2: $match->{line2}";
    }
    else {
        $string .= "Ln: $match->{line} - "   . ($match->{line} + $match->{len} - 1)
           . " Ln2: $match->{line2} - " . ($match->{line2} + $match->{len} - 1);
    }
    $string .= " Length: $match->{len}"
       . " Score: $match->{score}"
       . " File1: $match->{file}"
       . " File2: $match->{file2}";
    return $string;
}
sub report {
    my ($matches) = @_;
    for my $match (sort {$a->{score} <=> $b->{score}} @$matches) {
        print matchToString($match) . "\n";
    }
}
sub compare {
    my ($line1, $line2) = @_;
    #Both blank strings
    if ($line1 eq '' && $line2 eq '') {
        return 1.0;
    }
    #Prevent division by zero when dividing by shortest string later on
    elsif ($line1 eq '' || $line2 eq '') {
        return 0.0;
    }
    my $same = 0;
    my $total = length($line1) + length($line2);
    my $shortest = length($line1) < length($line2) ? length($line1) : length($line2);
    my $longest = length($line1) > length($line2) ? length($line1) : length($line2);
    my $i = 0;
    while ($i < $shortest) {
        if (substr($line1, $i, 1) eq substr($line2, $i, 1)) {
            $same++;
        }
        else {
            for my $j ($i + 1 .. ($shortest-1)) {
                if ((substr($line1, $i, 1) eq substr($line2, $j, 1)) ||
                    (substr($line1, $j, 1) eq substr($line2, $i, 1))) {
                    $same++;
                    $i = $j;
                    last;
                }
            }
        }
        $i++;
    }
    return $same / $longest;
}
sub compareFiles {
    my ($firstfile, $secondfile) = @_;
    open my $first, "<", $firstfile or die "Can't open $firstfile: $ERRNO\n";
    #Note the byte offsets of the beginning of each line in each file.
    my @offsets = (0);
    my $offset = 0;
    while (<$first>) {
        $offsets[$.] = $offset;
        $offset = tell $first;
    }
    my $size1 = $.;
    seek $first, 0, 0;
    open my $second, "<", $secondfile or die "Can't open $secondfile: $ERRNO\n";
    my @offsets2 = (0);
    $offset = 0;
    while (<$second>) {
        $offsets2[$.] = $offset;
        $offset = tell $second;
    }
    my $size2 = $.;
    seek $second, 0, 0;
    #Show sizes of first and second file
    #The funny ${} expression pluralizes the sizes if they aren't 1.
    print  "Size of $firstfile: $size1 line${$size1 == 1 ? \'' : \'s'}\n";
    print "Size of $secondfile: $size2 line${$size2 == 1 ? \'' : \'s'}\n";
    my ($linenumber, $linenumber2);
    my (@matches, @matches2);
    my $currentMatch;
    my $i = 0;
    for my $lineI (<$first>) {
        last if $requestexit;
        my $j = 0;
        seek $second, 0, 0;
        chomp $lineI;
        for my $lineJ (<$second>) {
            $linenumber = $i+1;
            $linenumber2= $j+1;
            chomp $lineJ;
            my $score = 0;
            if ($lineI eq $lineJ) {
                if ($firstfile ne $secondfile or $i != $j) {
                    $score = 1;
                }
            }
            else {
                $score = compare($lineI, $lineJ);
                if ($score > $threshold) {
                    print "Found similar match: $linenumber, $linenumber2\n";
                    print "LineI=\"$lineI\"\n";
                    print "LineJ=\"$lineJ\"\n";
                }
                else {
                    $score = 0;
                }
            }
            if ($score) {
                my $newMatch = {line => $linenumber,
                                line2 => $linenumber2,
                                file => $firstfile,
                                file2 => $secondfile,
                                len => 1,
                                score => $score};
                if (ref($currentMatch) ne 'HASH'
                    || ($currentMatch->{line}  + $currentMatch->{len} < $newMatch->{line} &&
                        $currentMatch->{line2} + $currentMatch->{len} < $newMatch->{line2})) {
                    #Read ahead to try to find more matching lines
                    my ($firstpos, $secondpos) = ($offsets[$i], $offsets2[$j]);
                    #I don't know why these two seeks are needed but removing them makes the program break
                    #$i+1 because seeking to offsets[$i] would just read the same line again.
                    seek $first, $offsets[$i + 1], SEEK_SET;
                    seek $second, $offsets[$j + 1], SEEK_SET;
                    while (($score = compare(<$first> // last, <$second> // last)) > $threshold) {
                        $newMatch->{len}++;
                        $newMatch->{score} += $score;
                    }
                    #Go back to position before reading ahead
                    seek $first, $firstpos, SEEK_SET;
                    seek $second, $secondpos, SEEK_SET;
                    #Add the new match to matches2
                    push @matches2, $newMatch;
                    $currentMatch = $newMatch;
                }
            }
            $j++;
        }
        $i++;
    }
    checkIfBrackets(\@matches2, \@matches, $first, $second, \@offsets, \@offsets2);
    close $second;
    close $first;
    return @matches;
}
sub finddupcode {
    my @matches;
    my ($filesref, $compareself) = @_;
    if ($compareself) {
        push @matches, map {compareFiles($_, $_)} @$filesref;
    }
    else {
        DUPES: for my $file (@$filesref) {
            last DUPES if $requestexit;
            next unless defined $file;
            push @matches, map {$file ne $_ ? compareFiles($file, $_) : ()} @$filesref;
        }
    }
    return @matches;
}
#Add to main @matches array only if the matching lines
#are something other than brackets/space
sub checkIfBrackets {
    my ($matches2, $matches, $first, $second, $offsets, $offsets2) = @_;
    for my $match (@$matches2) {
        if (ref($match) eq 'HASH') {
            my $lastLoc = tell $first;
            my $lastLoc2 = tell $second;
            seek $first, $offsets->[$match->{line} - 1], SEEK_SET;
            seek $second, $offsets2->[$match->{line2} - 1], SEEK_SET;
            my ($lines1, $lines2) = ("", "");
            my $currentMatch = $match;
            for my $i (1 .. $currentMatch->{len}) {
                my $firstLine = scalar readline $first;
                my $secondLine = scalar readline $second;
                chomp $firstLine;$firstLine =~ s/\r//g;
                chomp $secondLine;$secondLine =~ s/\r//g;
                $lines1 .= $firstLine;
                $lines2 .= $secondLine;
                #print "Line: $line $lines1, Line2: $line2 $lines2\n";
            }
            #Avoid adding the match if it is just brackets/space,
            #in which case it is probably the end of a function.
            unless ($lines1 =~ m/^[{}\s]*$/ &&
                    $lines2 =~ m/^[{}\s]*$/) {
                    push @$matches, $match;
            }
            seek $first, $lastLoc, SEEK_SET;
            seek $second, $lastLoc, SEEK_SET;
        }
    }
}
sub save {
    my ($matches, $filename) = @_;
    open my $fh, ">", $filename or die "Can't save results to file '$filename': $!";
    for my $match (sort {$a->{score} <=> $b->{score}} @$matches) {
        print {$fh} matchToString($match) . "\n";
    }
    close $fh;
}
sub quit {
    $requestexit=1;
}
1;
__END__
__C__
double compare(char* line1, char* line2) {
    /* Both blank strings */
    if (strcmp(line1, "") == 0 && strcmp(line2, "") == 0) {
        return 1.0;
    }
    /* Prevent division by zero when dividing by shortest string later on */
    else if (strcmp(line1, "") == 0 || strcmp(line2, "") == 0) {
        return 0.0;
    }
    int same = 0;
    int total = strlen(line1) + strlen(line2);
    int shortest = strlen(line1) < strlen(line2) ? strlen(line1) : strlen(line2);
    int longest = strlen(line1) > strlen(line2) ? strlen(line1) : strlen(line2);
    int i = 0;
    while (i < shortest) {
        if (line1[i] == line2[i]) {
            same++;
        }
        else {
            for (int j = i + 1;j < shortest - 1;j++) {
                if (line1[i] == line2[j] || line1[j] == line2[i]) {
                    same++;
                    i = j;
                    break;
                }
            }
        }
        i++;
    }
    return same/(longest + 0.0);
}